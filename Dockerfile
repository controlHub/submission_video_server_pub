# This is the dockerfile to build the lab image that can be used for
# lab development. It does not run a service on its own, the service needs
# to be run explicitly or through docker-compose.

FROM python:3.7.3

RUN pip install --upgrade pip

RUN pip install --upgrade opencv-python
#RUN pip install --upgrade numpy==1.13.3

RUN pip install --upgrade Babel==2.8.0 \
blinker==1.4 \
cachelib==0.1.1 \
certifi==2019.11.28 \
chardet==3.0.4 \
Click==7.0 \
Flask==1.1.1 \
Flask-Assets==2.0 \
Flask-Babel==2.0.0 \
Flask-DebugToolbar==0.11.0 \
flask-redis==0.4.0 \
Flask-Session==0.3.2 \
Flask-SocketIO==4.3.1 \
Flask-SQLAlchemy==2.4.4 \
Flask-Table==0.5.0 \
flask-wtf==0.14.3 \
idna==2.8 \
immutabledict==0.2.0 \
itsdangerous==1.1.0 \
Jinja2==2.11.1 \
MarkupSafe==1.1.1 \
mysql-connector-python==8.0.21 \
protobuf==3.13.0 \
PyMySQL==0.10.0 \
python-engineio==3.13.2 \
pytz==2020.1 \
redis==3.4.1 \
requests==2.22.0 \
six==1.14.0 \
SQLAlchemy==1.3.19 \
urllib3==1.25.8 \
webassets==2.0 \
weblablib==0.5.4 \
Werkzeug==0.16.1 \
wheel==0.34.2 \
WTForms==2.3.3



# This is an alternative to mounting our source code as a volume.
# Originally commented out because docker-compose adds the vol.
ADD . /app

ENV FLASK_DEBUG=1
ENV FLASK_APP=run.py
# To serve as a default value (autoapp.py)

#RUN pip install -r requirements.txt

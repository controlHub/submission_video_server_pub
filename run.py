from controlhub import create_app
import os

config_name = os.environ.get('FLASK_CONFIG') or 'default'

app = create_app(config_name)

if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=True, debug=True, port=5000)

from flask import render_template, Blueprint

main = Blueprint('main', __name__)


@main.route('/')
@main.route('/home')
def home():
    return render_template('home.html')


@main.route('/tech_data')
def tech_data():
    return render_template('tech_data.html')


@main.route('/about')
def about():
    return render_template('about.html')

from controlhub import db


class User(db.Model):
    """Data model for user accounts."""

    __tablename__ = 'Users'
    id = db.Column(db.Integer,
                   primary_key=True)
    username = db.Column(db.String(64),
                         index=False,
                         unique=True,
                         nullable=False)
    email = db.Column(db.String(80),
                      index=True,
                      unique=True,
                      nullable=False)
    created = db.Column(db.DateTime,
                        index=False,
                        unique=False,
                        nullable=False)

    def __repr__(self):
        return '<User {}>'.format(self.username)


class Submissions(db.Model):
    """Data model for user submissions."""

    __tablename__ = 'Submissions'
    id = db.Column(db.Integer,
                   primary_key=True)

    folder = db.Column(db.String(200),
                        index=True,
                        unique=True,
                        nullable=False)

    exp_type = db.Column(db.String(200),
                         index=False,
                         unique=False,
                         nullable=False)

    def __repr__(self):
        return '<Submission {}>'.format(self.folder)


class Control_quality(db.Model):
    """Data model for control quality."""

    __tablename__ = 'control_quality'
    id = db.Column(db.Integer,
                   primary_key=True)

    username = db.Column(db.String(30),
                       index=True,
                       unique=False,
                       nullable=False)

    folder = db.Column(db.String(30),
                         index=False,
                         unique=False,
                         nullable=False)

    date = db.Column(db.DateTime,
                       index=False,
                       unique=False,
                       nullable=False)

    l2norm_theta_sim = db.Column(db.Float,
                     index=False,
                     unique=False,
                     nullable=False)

    l2norm_alpha_sim = db.Column(db.Float,
                                 index=False,
                                 unique=False,
                                 nullable=False)

    l2norm_d_theta_sim = db.Column(db.Float,
                                 index=False,
                                 unique=False,
                                 nullable=False)

    l2norm_d_alpha_sim = db.Column(db.Float,
                                   index=False,
                                   unique=False,
                                   nullable=False)

    l2norm_u_sim = db.Column(db.Float,
                                   index=False,
                                   unique=False,
                                   nullable=False)


    linorm_theta_sim = db.Column(db.Float,
                                 index=False,
                                 unique=False,
                                 nullable=False)

    linorm_alpha_sim = db.Column(db.Float,
                                 index=False,
                                 unique=False,
                                 nullable=False)

    linorm_d_theta_sim = db.Column(db.Float,
                                   index=False,
                                   unique=False,
                                   nullable=False)

    linorm_d_alpha_sim = db.Column(db.Float,
                                   index=False,
                                   unique=False,
                                   nullable=False)

    linorm_u_sim = db.Column(db.Float,
                             index=False,
                             unique=False,
                             nullable=False)


    l2norm_theta_exp = db.Column(db.Float,
                                 index=False,
                                 unique=False,
                                 nullable=False)

    l2norm_alpha_exp = db.Column(db.Float,
                                 index=False,
                                 unique=False,
                                 nullable=False)

    l2norm_d_theta_exp = db.Column(db.Float,
                                   index=False,
                                   unique=False,
                                   nullable=False)

    l2norm_d_alpha_exp = db.Column(db.Float,
                                   index=False,
                                   unique=False,
                                   nullable=False)

    l2norm_u_exp = db.Column(db.Float,
                             index=False,
                             unique=False,
                             nullable=False)

    linorm_theta_exp = db.Column(db.Float,
                                 index=False,
                                 unique=False,
                                 nullable=False)

    linorm_alpha_exp = db.Column(db.Float,
                                 index=False,
                                 unique=False,
                                 nullable=False)

    linorm_d_theta_exp = db.Column(db.Float,
                                   index=False,
                                   unique=False,
                                   nullable=False)

    linorm_d_alpha_exp = db.Column(db.Float,
                                   index=False,
                                   unique=False,
                                   nullable=False)

    linorm_u_exp = db.Column(db.Float,
                             index=False,
                             unique=False,
                             nullable=False)

    exp_type = db.Column(db.String(50),
                             index=False,
                             unique=False,
                             nullable=False)


def __repr__(self):
    return '<control_quality {}>'.format(self.folder)

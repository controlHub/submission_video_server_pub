#from __future__ import unicode_literals, print_function, division
from flask import Flask
#from flask_assets import Environment
#from flask_babel import Babel
#from flask_debugtoolbar import DebugToolbarExtension
from flask_sqlalchemy import SQLAlchemy
from controlhub.config import config
from flask_session import Session
import time


db = SQLAlchemy()


def create_app(config_name):
    """
    This is a factory method. You can provide different setting names
    (development, testing, production) and it will initialize a Flask
    app with those settings. Check 'config.py' for further information.
    """
    app = Flask(__name__)
    config_class = config[config_name]
    print("{}: Using config: {}".format(time.asctime(), config_class.__name__))
    app.config.from_object(config_class)

    db.init_app(app)
    # other extensions
    #babel = Babel(app)
    #assets = Environment(app)
    #toolbar = DebugToolbarExtension(app)
    Session(app)

    from controlhub.main.routes import main
    from controlhub.experiment.routes import experiment
    from controlhub.video.routes import video
    from controlhub.tables.routes import table

    app.register_blueprint(main)
    app.register_blueprint(experiment)
    app.register_blueprint(video)
    app.register_blueprint(table)
    #db.create_all()

    return app


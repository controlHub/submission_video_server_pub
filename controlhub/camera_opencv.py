import cv2
from controlhub.base_camera import BaseCamera


class Camera(BaseCamera):
    video_source = 0
    frame_text = ''
    
    @staticmethod
    def set_video_source(source):
        Camera.video_source = source
    
    @staticmethod
    def impose_text(text):
        Camera.frame_text = text 
    
    @staticmethod
    def frames():
        camera = cv2.VideoCapture(Camera.video_source)
        if not camera.isOpened():
            raise RuntimeError('Could not start camera.')

        while True:
            # read current frame
            _, img = camera.read()

            position = (20, 450)
            text = Camera.frame_text
            font_scale = 0.5
            color = (0, 255, 255)
            thickness = 1
            font = cv2.FONT_HERSHEY_SIMPLEX
            line_type = cv2.LINE_AA

            text_size, _ = cv2.getTextSize(text, font, font_scale, thickness)
            line_height = text_size[1] + 5
            x, y0 = position
            for i, line in enumerate(text.split("\\n")):
                y = y0 + i * line_height
                cv2.putText(img,
                            line,
                            (x, y),
                            font,
                            font_scale,
                            color,
                            thickness,
                            line_type)

            # put text in the video
            #cv2.putText(img, Camera.frame_text, (20,450), font, 0.5, (0,255,255), 1, cv2.LINE_4)
            
            # encode as a jpeg image and return it
            yield cv2.imencode('.jpg', img)[1].tobytes()

from flask import render_template, Response, Blueprint
from flask import current_app
from controlhub.models import Submissions
from controlhub.camera_opencv import Camera

video = Blueprint('video', __name__)


@video.route('/video_page')
def video_page():
    """Video streaming home page."""
    resultset = Submissions.query
    return render_template('video_body.html', submissions=resultset[-8:], title='video')


def gen(camera, timer_directory):
    """Video streaming generator function."""
    while True:
        with open(timer_directory) as f:
            text_var = f.read()
        camera.impose_text(text_var)
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@video.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(Camera(), current_app.config['TIMER_DIR']),
                    mimetype='multipart/x-mixed-replace; boundary=frame')
from flask import url_for, render_template, session, request, redirect, flash, Blueprint
from flask import current_app
from datetime import datetime as dt
from controlhub.models import Submissions
from controlhub.experiment.forms import SubmissionForm
from controlhub import db
import os
import re

experiment = Blueprint('experiment', __name__)


@experiment.route('/exp_start')
def exp_start():
    return render_template('exp_start.html', title='experiment')


@experiment.route('/matlab')
def matlab():
    subm_address = current_app.config['SUBMISSION_ADDRESS']
    subm_folder = session['username'] + '!' + dt.now().strftime("%Y-%m-%d-%H-%M-%S")

    oldmask = os.umask(000)
    os.mkdir(subm_address + subm_folder, 0o777)
    os.umask(oldmask)

    with open(subm_address + subm_folder + '/' + 'init.m', 'wb+') as f1:
        f1.write(session['init'])
    with open(subm_address + subm_folder + '/' + 'control.m', 'wb+') as f2:
        f2.write(session['control'])
    with open(subm_address + subm_folder + '/' + 'observer.m', 'wb+') as f3:
        f3.write(session['observer'])

    # Create an instance of the Submission class
    print(session['exp_type'])
    submission_data = Submissions(folder=subm_folder, exp_type=session['exp_type'])
    db.session.add(submission_data)  # Adds new record to database
    db.session.commit()  # Commits all changes
    return redirect(url_for('video.video_page'))


@experiment.route('/exp_start/experiments', methods=["GET", "POST"])
def experiments():
    exp_type = request.args.get('exp_type')
    form = SubmissionForm()
    if form.is_submitted():
        print("submitted")
    if form.validate():
        print("valid")

    print(form.errors)

    if submit_limit():
        print('Submit limit is reached!')
        flash('Submit limit is reached for now, please wait!')
    else:
        print('Submit limit is not reached')
        if form.validate_on_submit():
            print("Validated!!!")
            if request.method == "POST":
                session['username'] = request.form["username"]
                session['init'] = bytes(request.form["mfile1"], encoding='utf-8')
                session['control'] = bytes(request.form["mfile2"], encoding='utf-8')
                session['observer'] = bytes(request.form["mfile3"], encoding='utf-8')
                session['exp_type'] = exp_type
                #session['exp_num'] = bytes(request.form["mult_run"], encoding='utf-8')
            return redirect(url_for('experiment.matlab'))
        else:
            print("Not validated!")
    return render_template('experiments.html', form=form, exp_type=exp_type, title='experiment')


def listdir_nohidden(path):
    files = os.listdir(path)
    return [i for i in files if not re.match('^\.', i)]


def submit_limit():
    files = listdir_nohidden(current_app.config['SUBMISSION_ADDRESS'])
    if len(files) > 10:
        return True
    return False
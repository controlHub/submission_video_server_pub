from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, SubmitField, IntegerField
from wtforms.validators import DataRequired, Length, Regexp, Optional, NumberRange


class SubmissionForm(FlaskForm):
    mfile1 = TextAreaField('mfile1', [
        DataRequired(),
        Regexp('^C_init=\[(.)*\]; *%*(.)*\sO_init=\[(.)*\]; *%*(.)*$', message=("Initialization is not correct, it must contain just 2 lines!"))],
        default='C_init=[0]; %Initialization of the controller internal state \nO_init=[0;0]; %Initialization of the observer internal state',
        id="ReadResult1")

    mfile2 = TextAreaField('mfile2', [
        DataRequired(),
        Regexp('^[\S\s]*$', message=("Controller is not correct!"))],
        default='function [u, C_out]=control(t, x,  x_ref, C_in)\n\
%-------------------------------------------------------------------------------------------\n\
%t - time\n\
%x=(theta,alpha,d_theta,d_alpha) is the system state (estimated by the observer),\n\
%where the angle alpha=0 corresponds to the lower position of the pendulum.\n\
%the variable C_in(out) is the internal variable of the controller (e.g. to realize integrator).\n\
%C_in corresponds to C_out at the previous instant of time.\n\
%The command C_out=C_in has to be included if the controller does not need an internal state.\n\
%-------------------------------------------------------------------------------------------\n\
%The following change of coordinate makes the state x(2)=0 corresponding to the upper\n\
%position of the pendulum.\n\
x(2)=mod (x(2),2*pi)-pi;\n\
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\
% THE USER-DEFINED CONTROLLER HAS TO BE REALIZED BELOW\n\
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\
C_out=C_in;\n\
%The static linear feedback u=K(x-x^ref) stabilizes the pendulum in the upper position\n\
%and tracks the reference theta^ref(t)=0.3sin(t).\n\
u=[2 -35 1.5 -3]*(x-x_ref);',
        id="ReadResult2")

    mfile3 = TextAreaField('mfile3', [
        DataRequired(),
        Regexp('^[\S\s]*$', message=("Observer is not correct!"))],
        default='function [x, O_out]=observer(t, y, u, O_in) \n\
%----------------------------------------------------------------------------------------- \n\
%t - time \n\
%y = (theta, alpha) - sensor output \n\
%u - the control input\n\
%x=(theta,alpha,d_theta,d_alpha) - the state estimate\n\
%O  is an internal variable observer (e.g. to realize a transfer function).\n\
%O_in corresponds to O_out in the previous instant of time, i.e. O_in=O(i-1) but O_out=O(i).\n\
%The command O_out=O_in has to be utilized if the observer does not need an internal state.\n\
%------------------------------------------------------------------------------------------\n\
%The observer given below estimates the time derivative of y as follows\n\
%  dy ~= W(s)y,\n\
% where  W(s)=(50s)/(s+50).\n\
% The algorithm is realized using implicit Euler discretization in two steps.\n\
%1) the update of the observer internal state:\n\
O_out=(0.0020*50*y+O_in)/(1+50*0.0020); % 0.0020 is the sampling period of the system\n\
%2) the estimate of the state:\n\
x=[y; 50*(y-O_out)];',
        id="ReadResult3")

    username = StringField('username', [
        DataRequired(),
        Length(min=3, max=10, message=('Username size must be in the interval [4, 10]')),
        Regexp('^[\w]+$', message=("Don't use any symbols different from Aa-Zz, _, 0-9"))], default="NaN")

    mult_run = IntegerField('mult_run', [
        Optional(),
        NumberRange(min=1, max=10)], default=1)
    submit = SubmitField('Submit')
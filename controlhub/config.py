import os
#from os import environ, path
# from dotenv import load_dotenv

#basedir = path.abspath(path.dirname(__file__))
#load_dotenv(path.join(basedir, '.env'))


class Config(object):
    # These URLs should change to customize your lab:
    SESSION_COOKIE_NAME = 'complete-session'
    SESSION_COOKIE_PATH = '/'
    SESSION_TYPE = 'filesystem'
    # General Flask Config
    SECRET_KEY = "yourcustomkeyhere"
    # Database
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    DEBUG = True
    # PC configuration
    SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://user:password@0.0.0.1:3306/databasename"
    SUBMISSION_ADDRESS = "~/submission_video_server/submission_files/"
    PROJECT_DIR = "~/ControlHub/submission_video_server/"
    TIMER_DIR = os.path.join(PROJECT_DIR, "controlhub/timer/timer.txt")


class UnitTestingConfig(Config):  # Pytests and unittests
    # Get the folder of the top-level directory of this project
    BASEDIR = os.path.abspath(os.path.dirname(__file__))

    SUBMISSION_ADDRESS = "~/submission_video_server/submission_files/"
    PROJECT_DIR = "~/submission_video_server/"
    TIMER_DIR = os.path.join(PROJECT_DIR, "controlhub/timer/timer.txt")
    TEST_DIR = os.path.join(PROJECT_DIR, "tests/")
    # Update later by using a random number generator and moving
    # the actual key outside of the source code under version control
    SECRET_KEY = 'bad_secret_key'
    DEBUG = True

    # SQLAlchemy
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(TEST_DIR, 'app_test.db')
    # SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://user:password@193.49.213.33:3306/db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Enable the TESTING flag to disable the error catching during request handling
    # so that you get better error reports when performing test requests against the application.
    TESTING = True

    # Disable CSRF tokens in the Forms (only valid for testing purposes!)
    WTF_CSRF_ENABLED = False


class ProductionConfig(Config):
    DEBUG = False
    # Main hardware configuration
    SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://user:password@0.0.0.0:3306/databasename"
    SUBMISSION_ADDRESS = "~/submission_video/submission_files/"
    PROJECT_DIR = "~/submission_video/"
    TIMER_DIR = os.path.join(PROJECT_DIR, "controlhub/timer/timer.txt")


config = {
    'default': DevelopmentConfig,
    'develop': DevelopmentConfig,
    'unittests': UnitTestingConfig,
    'production': ProductionConfig,
}

from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, RadioField, SubmitField
from wtforms.validators import Length, Regexp

COLUMNS = {'l2norm_theta_sim': r"\(||e^{sim}_{\theta}||_{L^{2}}\)",
           'l2norm_alpha_sim': r"\(||e^{sim}_{\alpha}||_{L^{2}}\)",
           'l2norm_d_theta_sim': r"\(||e^{sim}_{\theta'}||_{L^{2}}\)",
           'l2norm_d_alpha_sim': r"\(||e^{sim}_{\alpha'}||_{L^{2}}\)",
           'l2norm_u_sim': r"\(||u^{sim}||_{L^{2}}\)",

           'linorm_theta_sim': r"\(||e^{sim}_{\theta}||_{L^{\infty}}\)",
           'linorm_alpha_sim': r"\(||e^{sim}_{\alpha}||_{L^{\infty}}\)",
           'linorm_d_theta_sim': r"\(||e^{sim}_{\theta'}||_{L^{\infty}}\)",
           'linorm_d_alpha_sim': r"\(||e^{sim}_{\alpha'}||_{L^{\infty}}\)",
           'linorm_u_sim': r"\(||u^{sim}||_{L^{\infty}}\)",

           'l2norm_theta_exp': r"\(||e^{exp}_{\theta}||_{L^{2}}\)",
           'l2norm_alpha_exp': r"\(||e^{exp}_{\alpha}||_{L^{2}}\)",
           'l2norm_d_theta_exp': r"\(||e^{exp}_{\theta'}||_{L^{2}}\)",
           'l2norm_d_alpha_exp': r"\(||e^{exp}_{\alpha'}||_{L^{2}}\)",
           'l2norm_u_exp': r"\(||u^{exp}||_{L^{2}}\)",

           'linorm_theta_exp': r"\(||e^{exp}_{\theta}||_{L^{\infty}}\)",
           'linorm_alpha_exp': r"\(||e^{exp}_{\alpha}||_{L^{\infty}}\)",
           'linorm_d_theta_exp': r"\(||e^{exp}_{\theta'}||_{L^{\infty}}\)",
           'linorm_d_alpha_exp': r"\(||e^{exp}_{\alpha'}||_{L^{\infty}}\)",
           'linorm_u_exp': r"\(||u^{exp}||_{L^{\infty}}\)"}

COLS_DEF = ['l2norm_theta_exp', 'l2norm_u_exp', 'linorm_theta_exp','linorm_u_exp']


class TableForm(FlaskForm):
    #rows_limit = SelectField('rows_limit', choices=[('5', 5), ('10', 10), ('20', 20)], default=5)

    #page_num = IntegerField('page_num', [
    #    Optional(),
    #    NumberRange(min=1, max=10)], default=1)

    for key in COLUMNS:
        exec(key + " = BooleanField('{}')".format(key))

    #check_all = BooleanField('check_all')

    username = StringField('username', [
        Length(min=0, max=10, message=('Username size must be in the interval [0, 10]')),
        Regexp('^[0-9a-zA-Z]*$', message=("Don't use any symbols different from Aa-Zz, _, 0-9"))])

    submit = SubmitField('Submit')

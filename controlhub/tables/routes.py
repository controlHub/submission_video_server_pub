from flask import render_template, session, request, Blueprint
from .forms import COLUMNS, COLS_DEF, TableForm
from controlhub.models import Control_quality

table = Blueprint('table', __name__)


@table.route('/results')
def results():
    #cols = {}
    session['cols'] = {}
    session['rows_num'] = 10
    session['submname'] = ''
    #session['sort_col'] = ''
    #session['sort_dir'] = ''
    for key in COLUMNS:
        if key not in COLS_DEF:
            session['cols'][key] = "n"
        else:
            session['cols'][key] = "y"
    return render_template('results.html', title='results')


@table.route('/results/tables', methods=["GET", "POST"])
def tables():
    page = request.args.get('page', 1, type=int)
    exp_type = request.args.get('exp_type')
    sort_col = request.args.get('sort_col', 'id')
    print('sort_col = ', sort_col)
    sort_dir = request.args.get('sort_dir', 'desc')
    print('sort_dir = ', sort_dir)
    form = TableForm()

    if form.validate_on_submit():
        print('Validated!')
        if request.method == "POST":
            #session['rows_num'] = int(request.form["rows_limit"])
            for key in session['cols']:
                if request.form.get(key, False) == 'y':
                    session['cols'][key] = 'y'
                elif not request.form.get(key, False):
                    session['cols'][key] = 'n'
            username = request.form.get('username', False)
            if not username:
                session['submname'] = ''
            if username:
                session['submname'] = username
            data = Control_quality.query.filter_by(exp_type=exp_type).filter(
                    Control_quality.username.op('regexp')(r'({})+'.format(username))).order_by(
                    getattr(getattr(Control_quality, sort_col), sort_dir)()).paginate(page=page, per_page=session['rows_num'])
            return render_template('tables.html', form=form, exp_type=exp_type, sort_col=sort_col, sort_dir=sort_dir, data=data, columns=COLUMNS, cols_show=session['cols'], _anchor='table', title='table', submname=session['submname'])
    else:
        print("Not validated!")
    data = Control_quality.query.filter_by(exp_type=exp_type).filter(
        Control_quality.username.op('regexp')(r'({})+'.format(session['submname']))).order_by(
        getattr(getattr(Control_quality, sort_col), sort_dir)()).paginate(page=page, per_page=session['rows_num'])
    return render_template('tables.html', form=form, exp_type=exp_type, sort_col=sort_col, sort_dir=sort_dir, data=data, columns=COLUMNS, cols_show=session['cols'], _anchor="table", title='table', submname=session['submname'])


@table.route('/item/<int:id>')
def flask_link(id):
    element = Control_quality.query.get(id)
    print(element)
    link = "/static/exp_results/" + element.folder + ".zip"
    return render_template('download_page.html', Date=element.date, Username=element.username, Link=link, title='download')

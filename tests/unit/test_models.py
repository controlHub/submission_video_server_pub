"""
This file (test_models.py) contains the unittests for the database models.
"""


def test_new_user(new_user):
    """
    GIVEN a User model
    WHEN a new User is created
    THEN check the username, email and created date are defined correctly
    """
    assert new_user.username == 'Kennedy'
    assert new_user.email == 'patkennedy79@gmail.com'
    assert new_user.created == '2021-04-14-19-55-17'


def test_new_submission(new_submission):
    assert new_submission.folder == 'Kennedy!2021-04-14-19-55-17'
    assert new_submission.exp_type == 'stabilisation'


def test_new_result(new_result):
    assert new_result.username == 'Kennedy'
    assert new_result.date == '2021-04-14-19-55-17'
    assert new_result.folder =='Kennedy!2021-04-14-19-55-17'
    assert new_result.l2norm_theta_sim == float(0.1)
    assert new_result.l2norm_alpha_sim == float(0.1)
    assert new_result.l2norm_d_theta_sim == float(0.1)
    assert new_result.l2norm_d_alpha_sim == float(0.1)
    assert new_result.l2norm_u_sim == float(0.1)
    assert new_result.linorm_theta_sim == float(0.1)
    assert new_result.linorm_alpha_sim == float(0.1)
    assert new_result.linorm_d_theta_sim == float(0.1)
    assert new_result.linorm_d_alpha_sim == float(0.1)
    assert new_result.linorm_u_sim == float(0.1)
    assert new_result.l2norm_theta_exp == float(0.1)
    assert new_result.l2norm_alpha_exp == float(0.1)
    assert new_result.l2norm_d_theta_exp == float(0.1)
    assert new_result.l2norm_d_alpha_exp == float(0.1)
    assert new_result.l2norm_u_exp == float(0.1)
    assert new_result.linorm_theta_exp == float(0.1)
    assert new_result.linorm_alpha_exp == float(0.1)
    assert new_result.linorm_d_theta_exp == float(0.1)
    assert new_result.linorm_d_alpha_exp == float(0.1)
    assert new_result.linorm_u_exp == float(1)
    assert new_result.exp_type == 'stabilisation'

import os
import pytest
from controlhub import create_app, db
from controlhub.models import User, Submissions, Control_quality
from controlhub.experiment.forms import SubmissionForm
from controlhub.tables.forms import TableForm
from datetime import datetime


@pytest.fixture(scope='module')
def new_user():
    user = User(username='Kennedy', email='patkennedy79@gmail.com', created='2021-04-14-19-55-17')
    return user


@pytest.fixture(scope='module')
def new_submission():
    submission = Submissions(folder='Kennedy!2021-04-14-19-55-17', exp_type='stabilisation')
    return submission


@pytest.fixture(scope='module')
def new_result():
    result = Control_quality(username='Kennedy', date='2021-04-14-19-55-17',
                                       folder='Kennedy!2021-04-14-19-55-17',
                                          l2norm_theta_sim=float(0.1),
                                          l2norm_alpha_sim=float(0.1),
                                          l2norm_d_theta_sim=float(0.1),
                                          l2norm_d_alpha_sim=float(0.1),
                                          l2norm_u_sim=float(0.1),

                                          linorm_theta_sim=float(0.1),
                                          linorm_alpha_sim=float(0.1),
                                          linorm_d_theta_sim=float(0.1),
                                          linorm_d_alpha_sim=float(0.1),
                                          linorm_u_sim=float(0.1),

                                          l2norm_theta_exp=float(0.1),
                                          l2norm_alpha_exp=float(0.1),
                                          l2norm_d_theta_exp=float(0.1),
                                          l2norm_d_alpha_exp=float(0.1),
                                          l2norm_u_exp=float(0.1),

                                          linorm_theta_exp=float(0.1),
                                          linorm_alpha_exp=float(0.1),
                                          linorm_d_theta_exp=float(0.1),
                                          linorm_d_alpha_exp=float(0.1),
                                          linorm_u_exp=float(1),
                                          exp_type='stabilisation')
    return result


@pytest.fixture(scope='module')
def test_client():
    flask_app = create_app('Unittest')

    # Create a test client using the Flask application configured for testing
    with flask_app.test_client() as client:
        yield client


@pytest.fixture(scope='module')
def init_database(test_client):
    # Create the database and the database table
    db.create_all()

    # Insert user data
    user1 = User(username='Kennedy', email='patkennedy79@gmail.com', created=datetime(2021, 4, 14, 19, 55, 17))
    user2 = User(username='Arnold', email='arnold74@gmail.com', created=datetime(2021, 4, 14, 18, 35, 13))

    submission1 = Submissions(folder='Kennedy!2021-04-14-19-55-17', exp_type='stabilisation')

    control_quality1 = Control_quality(username='Kennedy', date=datetime(2021, 4, 14, 19, 55, 17),
                                       folder='Kennedy!2021-04-14-19-55-17',
                                          l2norm_theta_sim=float(0.1),
                                          l2norm_alpha_sim=float(0.1),
                                          l2norm_d_theta_sim=float(0.1),
                                          l2norm_d_alpha_sim=float(0.1),
                                          l2norm_u_sim=float(0.1),

                                          linorm_theta_sim=float(0.1),
                                          linorm_alpha_sim=float(0.1),
                                          linorm_d_theta_sim=float(0.1),
                                          linorm_d_alpha_sim=float(0.1),
                                          linorm_u_sim=float(0.1),

                                          l2norm_theta_exp=float(0.1),
                                          l2norm_alpha_exp=float(0.1),
                                          l2norm_d_theta_exp=float(0.1),
                                          l2norm_d_alpha_exp=float(0.1),
                                          l2norm_u_exp=float(0.1),

                                          linorm_theta_exp=float(0.1),
                                          linorm_alpha_exp=float(0.1),
                                          linorm_d_theta_exp=float(0.1),
                                          linorm_d_alpha_exp=float(0.1),
                                          linorm_u_exp=float(1),
                                          exp_type='stabilisation')

    db.session.add(user1)
    db.session.add(user2)
    db.session.add(submission1)
    db.session.add(control_quality1)
    # Commit the changes for the users
    db.session.commit()

    yield  # this is where the testing happens!

    db.drop_all()

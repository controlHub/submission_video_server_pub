"""
This file (test_routes.py) contains the functional tests for the all blueprint.

These tests use GETs and POSTs to different URLs to check for the proper behavior
of the blueprints.
"""


def test_home_page(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/home' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/')
    assert response.status_code == 200
    assert b'Rotary Inverted Pendulum' in response.data
    response = test_client.get('/home')
    assert response.status_code == 200
    assert b'Rotary Inverted Pendulum' in response.data


def test_home_page_post_with_fixture(test_client):
    """
    GIVEN a Flask application
    WHEN the '/' page is posted to (POST)
    THEN check that a '405' status code is returned
    """
    response = test_client.post('/')
    assert response.status_code == 405
    assert b"Rotary Inverted Pendulum" not in response.data


def test_tech_data_page(test_client):
    response = test_client.get('/tech_data')
    assert response.status_code == 200
    assert b'Downloads' in response.data


def test_exp_start_page(test_client):
    response = test_client.get('/exp_start')
    assert response.status_code == 200
    assert b'Please, choose the task type:' in response.data


def test_experiments_page(test_client, init_database):

    response = test_client.get('/exp_start/experiments?exp_type=stabilisation')
    assert response.status_code == 200
    assert b'stabilisation' in response.data
    response = test_client.get('/exp_start/experiments?exp_type=step_signal')
    assert response.status_code == 200
    assert b'step_signal' in response.data
    response = test_client.get('/exp_start/experiments?exp_type=ref_trajectory')
    assert response.status_code == 200
    assert b'ref_trajectory' in response.data


def test_video_page_page(test_client, init_database):
    response = test_client.get('/video_page')
    assert response.status_code == 200
    assert b'Online video' in response.data


def test_results_page(test_client):
    response = test_client.get('/results')
    assert response.status_code == 200
    assert b'Please, choose the task type you want to see the results for:' in response.data


'''
def test_tables_page(test_client, init_database):
    response = test_client.get('/results/tables?exp_type=stabilisation')
    assert response.status_code == 200
    response = test_client.get('/results/tables?exp_type=step_signal')
    assert response.status_code == 200
    response = test_client.get('/results/tables?exp_type=ref_trajectory')
    assert response.status_code == 200
'''


def test_about_page(test_client):
    response = test_client.get('/about')
    assert response.status_code == 200

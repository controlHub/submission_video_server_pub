# ControlHub. Submission_video_server

The Flask web-server for submissions handling and online video broadcasting

## Table of contents
* [General info](#General-info)
* [Features](#Features)
* [Technologies](#Technologies)
* [Linux setup](#Linux setup)
* [Database setup](#Database setup)
* [FTP setup](#FTP setup)
* [Webserver setup](#Webserver setup)

## General Info
The server is universal and developed for the laboratories specialised in control. It provides easy and fast service for testing and evaluation of the control algorithms.

## Features
- Several types of control trajectories
- Submission forms are patternised and completely safe
- Broadcasting the online video from the laboratory camera
- The status of the experiment execution broadcasted inside the video
- Easy access to the results table 


## Technologies
Project is created with:
* Python 3.6 or 3.8
* Flask 1.1.1
* Bootstrap 4
* Docker
	
## Linux setup
```
$ ssh root@192.168.1.43
$ apt update
$ hostnamectl set-hostname controlhub
$ hoostname
$ nano /etc/hosts ## write ip and hostname (192.168.1.43   controlhub)
$ adduser fiodar ## password...
$ adduser fiodar sudo
$ exit
$ ssh fiodar@192.168.1.43 ## password
$ pwd
$ mkdir .ssh

### now generate ssh key on the main machine and copy to the linux machine
$ ssh-keygen -b 4096
$ scp ~/.ssh/id_rsa.pub fiodar@192.168.1.43:~/.ssh/authorized_keys
###

$ ls .ssh
$ sudo chmod 700 ~/.ssh/
$ sudo chmod 600 ~/.ssh/*
$ exit

$ ssh fiodar@192.168.1.43
$ sudo nano /etc/ssh/sshd_config # password
# PermitRootLogin no
# PasswordAuthentication no
$ sudo systemctl restart sshd
$ sudo apt install ufw
$ sudo ufw default allow outgoing
$ sudo ufw default deny incoming
$ sudo ufw allow ssh
$ sudo ufw allow 5000
$ sudo ufw enable
$ sudo ufw status # 22/tcp, 5000, 22/tcp v6, 5000 v6

### downloading your server with requirements.txt
$  pip freeze > requirements.txt
$  scp -r Desktop/controlhub fiodar@192.168.1.43:~/
###

$ sudo apt install python3-pip
$ sudo apt install python3-venv
$ python3 -m venv controlhub/venv
$ cd controlhub
$ ls
$ source venv/bin/activate
$  (venv)
$ pip install -r requirements.txt

python
# import is
# os.environ.get('SECRET_KEY')

$ export FLASK_APP=run.py
$ flask run --host=0.0.0.0

### nginx + gunicorn
$ sudo apt install nginx
$ pip install gunicorn
$ sudo rm /etc/nginx/sites-enabled/default
$ sudo nano /etc/nginx/sites-enabled/controlhub
# server 
# {listen 80; server_name 192.168.1.43; location /static 
# {alias /home/fiodar/submission_video/controlhub/static;}
# location / {proxy_pass http://localhost:8000;include /etc/nginx/proxy_params; proxy_redirect off;}}
###

$ sudo ufw allow http/tcp
$ sudo ufw delete allow 5000
$ sudo ufw enable
$ sudo systemctl restart nginx
$ nproc --all
$ gunicorn -w 3 run:app
$ sudo apt install supervisor
$ sudo nano /etc/supervisor/conf.d/controlhub.conf
# [program:controlhub]
# directory=/home/fiodar/submission_video
# command=/home/fiodar/submission_video/venv/bin/gunicorn -w 3 run:app
# user=coreyms
# autostart=true
# autorestart=true
# stopasgroup=true
# killasgroup=true
# stderr_logfile=/var/log/controlhub/controlhub.err.log
# stdout_logfile=/var/log/controlhub/controlhub.out.log
$ sudo mkdir -p /var/log/controlhub
$ sudo touch /var/log/controlhub/controlhub.err.log
$ sudo touch /var/log/controlhub/controlhub.out.log
$ sudo supervisorctl reload
```
## Database setup
```
$ sudo apt install mariadb-server
$ sudo mysql_secure_installation
$ sudo mysql -u root -p
$ quit;
$ sudo mysql -u root -p
$ CREATE DATABASE exampledb;
$ CREATE USER 'user'@'%' IDENTIFIED BY 'password';
$ GRANT ALL PRIVILEGES ON exampledb.* TO 'user'@'%';
$ FLUSH PRIVILEGES;
$ quit;

$ sudo nano /etc/mysql/mariadb.conf.d/50-server.cnf
###-->
bind-address = 0.0.0.0
###<--

$ sudo systemctl restart mariadb.service
$ sudo ufw allow 3306/tcp
```
## FTP setup
```
$ service vsftpd status
$ sudo apt purge vsftpd
$ sudo apt-get install vsftpd
$ systemctl start vsftpd
$ system enable vsftpd

$ apt install ufw
$ ufw allow 20/tcp
$ ufw allow 21/tcp

$ service vsftpd status

$ nano /etc/vsftpd.conf

###-->
listen=YES
#listen_ipv6=YES
write_enable=YES
idle_session_timeout=120
chroot_local_user=YES
allow_writeable_chroot=YES
###<--

$ service vsftpd restart
$ service vsftpd start

# create a user with FTP access
$ adduser fiodar
$ mkdir /home/fiodar/ftp
$ chown /home/fiodar/ftp
```
## Webserver setup
To run this project, copy it to the local folder, specify your variables in the config.py file and run using python 3.6 or higher:

```
$ cd ../submission_video_server/
$ python run.py
```
